const BUTTON_GROUP = document.querySelectorAll('.button');

const BOOM_SOUND = new Audio('./../sounds/boom.wav');
const CLAP_SOUND = new Audio('./../sounds/clap.wav');
const HIHAT_SOUND = new Audio('./../sounds/hihat.wav');
const KICK_SOUND = new Audio('./../sounds/kick.wav');
const OPENHAT_SOUND = new Audio('./../sounds/openhat.wav');
const RIDE_SOUND = new Audio('./../sounds/ride.wav');
const SNARE_SOUND = new Audio('./../sounds/snare.wav');
const TINK_SOUND = new Audio('./../sounds/tink.wav');
const TOM_SOUND = new Audio('./../sounds/tom.wav');

BUTTON_GROUP.forEach(button => {
    button.addEventListener('mousedown', playSound);
    button.addEventListener('mousedown', stylizeButton);
    button.addEventListener('mouseup', unstylizeButton);
})

document.addEventListener('keydown', handleKeyDown);
document.addEventListener('keyup', handleKeyUp);

function handleKeyDown(e){
    let button = document.querySelector('div[data-key="' + e.keyCode + '"]');
    
    stylizeButton.call(button);
    playSound.call(button);
}

function handleKeyUp(e){
    let button = document.querySelector('div[data-key="' + e.keyCode + '"]');
    
    unstylizeButton.call(button);
}

function playSound(){
    let soundName = this.querySelector('div:last-child').textContent;
    let soundVarName = soundName.toUpperCase() + '_' + 'SOUND';
    eval(soundVarName + ".play()");
}

function stylizeButton(){
    this.classList.add('button--active');
}

function unstylizeButton(){
    this.classList.remove('button--active');
}